/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Array1Component } from './Array1.component';

describe('Array1Component', () => {
  let component: Array1Component;
  let fixture: ComponentFixture<Array1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Array1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Array1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
