import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-Array1',
  templateUrl: './Array1.component.html',
  styleUrls: ['./Array1.component.css']
})
export class Array1Component implements OnInit {

  fruits: string[] = ["apple", "banana", "orange","papaya","pomogranite"];

  constructor() { 
  }

  ngOnInit() {
    this.fruits = this.fruits.concat("grape","watermelon");
  }

}
