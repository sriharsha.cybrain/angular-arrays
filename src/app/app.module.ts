import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Array1Component } from './Array1/Array1.component';
import { Array2Component } from './Array2/Array2.component';

@NgModule({
  declarations: [			
    AppComponent,
      Array1Component,
      Array2Component
   ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
