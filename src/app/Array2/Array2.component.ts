import { Component, OnInit } from '@angular/core';

export class Stack<T> {
  private items: T[];

  constructor() {
    this.items = [];
  }

  push(item: T) {
    this.items.push(item);
  }

  pop(): T | undefined {
    return this.items.pop();
  }

  isEmpty(): boolean {
    return this.items.length === 0;
  }

  peek(): T | undefined {
    return this.items[this.items.length - 1];
  }
}
interface Person {
  Id: number;
  FirstName: string;
  LastName: string;
  Email: string;
  ContactNo: string;
}


@Component({
  selector: 'app-Array2',
  templateUrl: './Array2.component.html',
  styleUrls: ['./Array2.component.css']
})
export class Array2Component implements OnInit {
  //2D ARRAY
DataSet: { Id: number, FirstName: string, LastName: string, Email: string, ContactNo: string }[] = [
  { Id: 1, FirstName: 'Chandler', LastName: 'Bing', Email: 'cb@example.com', ContactNo: '1234567890' },
  { Id: 2, FirstName: 'Monica', LastName: 'geller', Email: 'mg@example.com', ContactNo: '2345678901' },
  { Id: 3, FirstName: 'Pheobe', LastName: 'buffay', Email: 'pb@example.com', ContactNo: '3456789012' },
  { Id: 4, FirstName: 'Joey', LastName: 'Tribbiani', Email: 'jt@example.com', ContactNo: '4567890123' }
];


DataSet1: Person[] = [
  { Id: 1, FirstName: 'iron', LastName: 'man', Email: 'im@example.com', ContactNo: '12345887899' },
  { Id: 2, FirstName: 'incredible', LastName: 'hulk', Email: 'ih@example.com', ContactNo: '2145578901' },
  { Id: 3, FirstName: 'ant', LastName: 'man', Email: 'am@example.com', ContactNo: '3456258013' },
  { Id: 4, FirstName: 'black', LastName: 'panther', Email: 'bp@example.com', ContactNo: '7412890123' }
];


//PUSH OPERATION
push=this.DataSet.push(
  { Id: 5, FirstName: 'John', LastName: 'Doe', Email: 'johndoe@example.com', ContactNo: '555-555-5555' },
  { Id: 6, FirstName: 'Jane', LastName: 'Doe', Email: 'janedoe@example.com', ContactNo: '555-555-5555' },
  { Id: 7, FirstName: 'Bob', LastName: 'Smith', Email: 'bobsmith@example.com', ContactNo: '555-555-5555' }
);

//6.
newElement = { Id: 8, FirstName: 'Rachel', LastName: 'Green', Email: 'rg@example.com', ContactNo: '5678901234' };

//7.
 arr1 = [1, 2, 3];
 arr2 = [4, 5, 6];

//8.
removedElement = this.DataSet.pop();

//10.
myArray = [];

//12.
myStack: Stack<number> = new Stack<number>();

//13.
myArray1: number[] = [1, 2, 3, 4, 5];

//14.
myArray2 = [1, 2, 3, 4, 5, 6, 7, 8, 9];
startIndex = 3;
numberOfElementsToRemove = 4;

//15.
myArray3 = [5,4,3,2,1];
removedElements = this.myArray3.splice(1, 2);

//16.
myArray4 = [11, 22, 33, 44, 55, 66, 77, 88, 99];
indexToReplace = 2;
newItem = 6;

//18.
originalArray = [111, 2222, 3333, 4444, 5555];
DuplicateArray = this.originalArray.slice();

//20.
Reyna = this.DataSet.map(item => item.Id);


constructor() { 

  //12.
    this.myStack.push(1);
    this.myStack.push(2);
    this.myStack.push(3);

    console.log(this.myStack.pop()); 
    console.log(this.myStack.pop()); 
    console.log(this.myStack.peek());
    console.log(this.myStack.isEmpty());
}

  ngOnInit() {
    // 4. The push() function returns the new length of the array after adding the new elements
    const length = this.DataSet.length;
    console.log(length);

    // 5. No, the push() function is used to add elements to the end of an array.

    // 6.
    if (!this.DataSet.some(element => element.Id === this.newElement.Id)) {
      this.DataSet.push(this.newElement);
    }

    //7.
    this.arr1.push(...this.arr2);


    //8.
    console.log(this.DataSet);
    console.log(this.removedElement);


    //9. The pop() function returns the removed element from the end of an array.


    //10.
    if (this.myArray.pop() === undefined) {
      console.log("The array is empty.");
    } else {
      console.log("The array is not empty.");
    }

    //11. No, the pop() function can only be used to remove the last element from an array. If you want to remove elements from the beginning of an array, you should use the shift() function instead.

    //13.
    this.myArray1.splice(1, 2);
    console.log(this.myArray1);


    //14.
    this.myArray2.splice(this.startIndex, this.numberOfElementsToRemove);
    console.log(this.myArray2);

    //15.
    console.log(this.myArray3);
    console.log(this.removedElements); 

    //16.
    this.myArray4.splice(this.indexToReplace, 1, this.newItem);
    console.log(this.myArray4);

    // 17.The slice() function is used to create a shallow copy of a portion of an 
    // array into a new array without modifying the original array.

    //The splice() function is used to add, remove, or replace elements in an array.


    //18.
    console.log(this.DuplicateArray)

    //19.
    this.DataSet.sort((a, b) => b.Id - a.Id);
    console.log(this.DataSet);

    //20.
    console.log(this.Reyna);
  }

}
